import requests
import json
import re


reg_standort= r'Standort (?P<standort>.*) –.*'

def get_old_last_minute_courses() -> list:
    response = requests.get('https://noordsestern.gitlab.io/thw-kurse-melden/last_minute.json')
    response.raise_for_status()
    old_last_minute_courses = json.loads(response.text)
    #return old_last_minute_courses
    return []

def create_notification(course: dict) -> str:
    standort_kurz = re.search(reg_standort, course["standort"]).group("standort")
    return f"""**{course["meldung"]}**
    
Lehrgang: {course["kurs"]}
In: {standort_kurz}
Von: {course["beginn"]}
Bis: {course["ende"]}

Inhalt: {course["kurs_link"]}
Anmeldung: {course["anmeldung_link"]}"""


def notify(courses: list):
    notifications = [create_notification(course) for course in courses]
    concatinated_notification = '\n--------------------------------------------\n'.join(notifications)
    print(concatinated_notification)


def print_hi(name):
    response = requests.get("https://noordsestern.gitlab.io/thw-kurse/last_minute.json")
    response.raise_for_status()
    last_minute_courses = json.loads(response.text)
    old_last_minute_courses = get_old_last_minute_courses()
    new_courses = []
    for course in last_minute_courses:
        del course["gelesen_am"]
        if course in old_last_minute_courses:
            continue
        new_courses.append(course)
    notify(new_courses)

    with open('last_minute.json', 'w') as json_file:
        json.dump(last_minute_courses, json_file, indent=4)


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print_hi('PyCharm')

